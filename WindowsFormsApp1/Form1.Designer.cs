﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.cButton = new System.Windows.Forms.Button();
            this.ceButton = new System.Windows.Forms.Button();
            this.digitOneButton = new System.Windows.Forms.Button();
            this.digitTwoButton = new System.Windows.Forms.Button();
            this.digitThreeButton = new System.Windows.Forms.Button();
            this.digitFourButton = new System.Windows.Forms.Button();
            this.digitFiveButton = new System.Windows.Forms.Button();
            this.digitSixButton = new System.Windows.Forms.Button();
            this.digitSevenButton = new System.Windows.Forms.Button();
            this.digitEightButton = new System.Windows.Forms.Button();
            this.digitNineButton = new System.Windows.Forms.Button();
            this.digitZeroButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.timesButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.equalsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox.Location = new System.Drawing.Point(33, 67);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.Size = new System.Drawing.Size(268, 36);
            this.textBox.TabIndex = 0;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(30, 42);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 13);
            this.label.TabIndex = 1;
            // 
            // cButton
            // 
            this.cButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cButton.Location = new System.Drawing.Point(33, 118);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(58, 55);
            this.cButton.TabIndex = 2;
            this.cButton.Text = "C";
            this.cButton.UseVisualStyleBackColor = true;
            this.cButton.Click += new System.EventHandler(this.cButton_Click);
            // 
            // ceButton
            // 
            this.ceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ceButton.Location = new System.Drawing.Point(97, 118);
            this.ceButton.Name = "ceButton";
            this.ceButton.Size = new System.Drawing.Size(58, 55);
            this.ceButton.TabIndex = 3;
            this.ceButton.Text = "CE";
            this.ceButton.UseVisualStyleBackColor = true;
            this.ceButton.Click += new System.EventHandler(this.ceButton_Click);
            // 
            // digitOneButton
            // 
            this.digitOneButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitOneButton.Location = new System.Drawing.Point(33, 179);
            this.digitOneButton.Name = "digitOneButton";
            this.digitOneButton.Size = new System.Drawing.Size(58, 55);
            this.digitOneButton.TabIndex = 4;
            this.digitOneButton.Text = "1";
            this.digitOneButton.UseVisualStyleBackColor = true;
            this.digitOneButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitTwoButton
            // 
            this.digitTwoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitTwoButton.Location = new System.Drawing.Point(97, 179);
            this.digitTwoButton.Name = "digitTwoButton";
            this.digitTwoButton.Size = new System.Drawing.Size(58, 55);
            this.digitTwoButton.TabIndex = 5;
            this.digitTwoButton.Text = "2";
            this.digitTwoButton.UseVisualStyleBackColor = true;
            this.digitTwoButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitThreeButton
            // 
            this.digitThreeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitThreeButton.Location = new System.Drawing.Point(161, 179);
            this.digitThreeButton.Name = "digitThreeButton";
            this.digitThreeButton.Size = new System.Drawing.Size(58, 55);
            this.digitThreeButton.TabIndex = 6;
            this.digitThreeButton.Text = "3";
            this.digitThreeButton.UseVisualStyleBackColor = true;
            this.digitThreeButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitFourButton
            // 
            this.digitFourButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitFourButton.Location = new System.Drawing.Point(33, 240);
            this.digitFourButton.Name = "digitFourButton";
            this.digitFourButton.Size = new System.Drawing.Size(58, 55);
            this.digitFourButton.TabIndex = 7;
            this.digitFourButton.Text = "4";
            this.digitFourButton.UseVisualStyleBackColor = true;
            this.digitFourButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitFiveButton
            // 
            this.digitFiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitFiveButton.Location = new System.Drawing.Point(97, 240);
            this.digitFiveButton.Name = "digitFiveButton";
            this.digitFiveButton.Size = new System.Drawing.Size(58, 55);
            this.digitFiveButton.TabIndex = 8;
            this.digitFiveButton.Text = "5";
            this.digitFiveButton.UseVisualStyleBackColor = true;
            this.digitFiveButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitSixButton
            // 
            this.digitSixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitSixButton.Location = new System.Drawing.Point(161, 240);
            this.digitSixButton.Name = "digitSixButton";
            this.digitSixButton.Size = new System.Drawing.Size(58, 55);
            this.digitSixButton.TabIndex = 9;
            this.digitSixButton.Text = "6";
            this.digitSixButton.UseVisualStyleBackColor = true;
            this.digitSixButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitSevenButton
            // 
            this.digitSevenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitSevenButton.Location = new System.Drawing.Point(33, 301);
            this.digitSevenButton.Name = "digitSevenButton";
            this.digitSevenButton.Size = new System.Drawing.Size(58, 55);
            this.digitSevenButton.TabIndex = 10;
            this.digitSevenButton.Text = "7";
            this.digitSevenButton.UseVisualStyleBackColor = true;
            this.digitSevenButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitEightButton
            // 
            this.digitEightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitEightButton.Location = new System.Drawing.Point(97, 301);
            this.digitEightButton.Name = "digitEightButton";
            this.digitEightButton.Size = new System.Drawing.Size(58, 55);
            this.digitEightButton.TabIndex = 11;
            this.digitEightButton.Text = "8";
            this.digitEightButton.UseVisualStyleBackColor = true;
            this.digitEightButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitNineButton
            // 
            this.digitNineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitNineButton.Location = new System.Drawing.Point(161, 301);
            this.digitNineButton.Name = "digitNineButton";
            this.digitNineButton.Size = new System.Drawing.Size(58, 55);
            this.digitNineButton.TabIndex = 12;
            this.digitNineButton.Text = "9";
            this.digitNineButton.UseVisualStyleBackColor = true;
            this.digitNineButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // digitZeroButton
            // 
            this.digitZeroButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitZeroButton.Location = new System.Drawing.Point(97, 362);
            this.digitZeroButton.Name = "digitZeroButton";
            this.digitZeroButton.Size = new System.Drawing.Size(58, 55);
            this.digitZeroButton.TabIndex = 13;
            this.digitZeroButton.Text = "0";
            this.digitZeroButton.UseVisualStyleBackColor = true;
            this.digitZeroButton.Click += new System.EventHandler(this.digitButton_Click);
            // 
            // plusButton
            // 
            this.plusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusButton.Location = new System.Drawing.Point(243, 118);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(58, 55);
            this.plusButton.TabIndex = 14;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.action_click);
            // 
            // minusButton
            // 
            this.minusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minusButton.Location = new System.Drawing.Point(243, 179);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(58, 55);
            this.minusButton.TabIndex = 15;
            this.minusButton.Text = "-";
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.action_click);
            // 
            // timesButton
            // 
            this.timesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesButton.Location = new System.Drawing.Point(243, 240);
            this.timesButton.Name = "timesButton";
            this.timesButton.Size = new System.Drawing.Size(58, 55);
            this.timesButton.TabIndex = 16;
            this.timesButton.Text = "*";
            this.timesButton.UseVisualStyleBackColor = true;
            this.timesButton.Click += new System.EventHandler(this.action_click);
            // 
            // divideButton
            // 
            this.divideButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divideButton.Location = new System.Drawing.Point(243, 301);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(58, 55);
            this.divideButton.TabIndex = 17;
            this.divideButton.Text = "/";
            this.divideButton.UseVisualStyleBackColor = true;
            this.divideButton.Click += new System.EventHandler(this.action_click);
            // 
            // equalsButton
            // 
            this.equalsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalsButton.Location = new System.Drawing.Point(243, 362);
            this.equalsButton.Name = "equalsButton";
            this.equalsButton.Size = new System.Drawing.Size(58, 55);
            this.equalsButton.TabIndex = 18;
            this.equalsButton.Text = "=";
            this.equalsButton.UseVisualStyleBackColor = true;
            this.equalsButton.Click += new System.EventHandler(this.equalsButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 436);
            this.Controls.Add(this.equalsButton);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.timesButton);
            this.Controls.Add(this.minusButton);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.digitZeroButton);
            this.Controls.Add(this.digitNineButton);
            this.Controls.Add(this.digitEightButton);
            this.Controls.Add(this.digitSevenButton);
            this.Controls.Add(this.digitSixButton);
            this.Controls.Add(this.digitFiveButton);
            this.Controls.Add(this.digitFourButton);
            this.Controls.Add(this.digitThreeButton);
            this.Controls.Add(this.digitTwoButton);
            this.Controls.Add(this.digitOneButton);
            this.Controls.Add(this.ceButton);
            this.Controls.Add(this.cButton);
            this.Controls.Add(this.label);
            this.Controls.Add(this.textBox);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button ceButton;
        private System.Windows.Forms.Button digitOneButton;
        private System.Windows.Forms.Button digitTwoButton;
        private System.Windows.Forms.Button digitThreeButton;
        private System.Windows.Forms.Button digitFourButton;
        private System.Windows.Forms.Button digitFiveButton;
        private System.Windows.Forms.Button digitSixButton;
        private System.Windows.Forms.Button digitSevenButton;
        private System.Windows.Forms.Button digitEightButton;
        private System.Windows.Forms.Button digitNineButton;
        private System.Windows.Forms.Button digitZeroButton;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button timesButton;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button equalsButton;
    }
}

