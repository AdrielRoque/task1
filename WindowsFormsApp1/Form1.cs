﻿/* 
 * Program ID: InClass1
 * 
 * Revision History:
 *        CREATED: Adriel Roque June 2, 2020
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Nullable<double> x = null;
        string pendingAction = null;
        bool newEquation = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

       
        private void ceButton_Click(object sender, EventArgs e)
        {
            textBox.Clear();
        }

        private void cButton_Click(object sender, EventArgs e)
        {
            textBox.Clear();
            label.Text = "";
            x = null;
            pendingAction = null;
            newEquation = true;
        }
        private void equalsButton_Click(object sender, EventArgs e)
        {
            switch(pendingAction)
            {
                case "+":
                    label.Text = label.Text + textBox.Text + "=";
                    textBox.Text = (x + double.Parse(textBox.Text)).ToString();
                    x = null;
                    pendingAction = null;
                    newEquation = true;
                    break;
                case "-":
                    label.Text = label.Text + textBox.Text + "=";
                    textBox.Text = (x - double.Parse(textBox.Text)).ToString();
                    x = null;
                    pendingAction = null;
                    newEquation = true;
                    break;
                case "*":
                    label.Text = label.Text + textBox.Text + "=";
                    textBox.Text = (x * double.Parse(textBox.Text)).ToString();
                    x = null;
                    pendingAction = null;
                    newEquation = true;
                    break;
                case "/":
                    label.Text = label.Text + textBox.Text + "=";
                    textBox.Text = (x / double.Parse(textBox.Text)).ToString();
                    x = null;
                    pendingAction = null;
                    newEquation = true;
                    break;
            }
        }

        

        private void digitButton_Click(object sender, EventArgs e)
        {
            if(newEquation)
            {
                textBox.Clear();
            }
            newEquation = false;
            Button digit = (Button)sender;
            textBox.Text = textBox.Text + digit.Text;
            
        }
        private void action_click(object sender, EventArgs e)
        {
            if (textBox.Text!="")
            {
                Button digit = (Button)sender;
                pendingAction = digit.Text;
                x = double.Parse(textBox.Text);
                label.Text = x + pendingAction;
                newEquation = true;
            }

        }
    }
}
