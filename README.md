The code is written in C# and is a windows form. It is a demonstration of a basic calculator

---

## Requirements

1. Microsoft Visual Studio
2. .Net Framework

## How to download/install the source code

1. Go to the Downloads on the overview page of the repository
2. Click the Download repository from the downloads tab
3. Extract the downloaded compressed file
4. Open the WindowsFormsApp1.sln file in Visual Studio

## License

MIT License

Copyright (c) 2021 Adriel Roque

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Note

The reasons for choosing the MIT license are:
1. It is a good open source license
2. It is short and simple
3. Few restrictions
4. One of the top open source licenses